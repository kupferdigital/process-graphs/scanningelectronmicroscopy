# scanningelectronmicroscopy
[![OntoFlow](https://gitlab.com/kupferdigital/process-graphs/scanningelectronmicroscopy/badges/main/pipeline.svg?style=flat-square&ignore_skipped=true&key_text=OntoFlow&key_width=80)](https://kupferdigital.gitlab.io/process-graphs/scanningelectronmicroscopy)
[![Graph](https://gitlab.com/kupferdigital/process-graphs/scanningelectronmicroscopy/badges/main/pipeline.svg?style=flat-square&ignore_skipped=true&key_text=Turtle&key_width=80)](https://kupferdigital.gitlab.io/process-graphs/scanningelectronmicroscopy/index.ttl)

A simple representation of the experiment steps needed to capture an image of an ROI in scanning electron microscopy.


<img src="https://kupferdigital.gitlab.io/process-graphs/scanningelectronmicroscopy/scanningelectronmicroscopy.svg">
